// App logic.
window.myApp = {};

document.addEventListener("init", function(event) {
  var page = event.target;

  // Each page calls its own initialization controller.
  if (myApp.controllers.hasOwnProperty(page.id)) {
    myApp.controllers[page.id](page);
  }

  // Fill the lists with initial data when the pages we need are ready.
  // This only happens once at the beginning of the app.
  if (
    page.id === "menuPage" ||
    page.id === "pendingTasksPage" ||
    page.id === "completedTasksPage"
  ) {
    if (
      document.querySelector("#menuPage") &&
      document.querySelector("#pendingTasksPage") &&
      document.querySelector("#enCours-list") &&
      !document.querySelector("#pendingTasksPage ons-list-item")
    ) {
      document.querySelector("#deleteAll").addEventListener("click", () => {
        var dialog = document.getElementById("my-alert-dialog");

        if (dialog) {
          dialog.show();
        } else {
          ons
            .createElement("alert-dialog.html", { append: true })
            .then(function(dialog) {
              dialog.show();

              let cancel = document.querySelector("#cancelDial");
              cancel.addEventListener("click", () => {
                document.getElementById("my-alert-dialog").hide();
              });

              let accept = document.querySelector("#aproove");
              accept.addEventListener("click", () => {
                document.getElementById("my-alert-dialog").hide();
                myApp.services.fixtures = {};

                let store = window.localStorage;
                store.setItem("save", JSON.stringify(myApp.services.fixtures));

                location.reload();
              });

            });
        }
      });

      let store = window.localStorage;
      let item = store.getItem("save");
      if (item) {
        myApp.services.fixtures = JSON.parse(item);
      }
      
      if (myApp.services.fixtures) {
        myApp.services.fixtures.forEach(function(data) {
          myApp.services.tasks.create(data);
        });
      }
    }
  }
});
